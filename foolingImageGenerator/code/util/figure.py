"""This module can be used to create figures/plots.
"""

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.offsetbox import AnchoredText
from scipy import interpolate
from enums.dataType import DataType
from util.customWarnings import CustomWarnings
from util.fileManager import FileManager as fm


class Figure:

    @staticmethod
    def beautify_string(string):
        return string.replace("_", " ").replace(".", ",")

    @staticmethod
    def save_figure(figure, figure_name, data, metadata):
        figure_name = fm.build_name(figure_name)
        fm.save(DataType.PLOT, figure, figure_name, data=data, metadata=metadata)

    @staticmethod
    def draw_plots(coordinates, diagram_name, x_label, y_label, mutation_names=[""], parameters=None, title=None,
                   x_min_max=None, y_min_max=None, show_dots=False, smooth=True, threshold=None):
        plt.close('all')
        figure, axis = plt.subplots()
        if title is None:
            title = diagram_name
        if x_min_max:
            plt.xticks(np.arange(x_min_max[0], x_min_max[1] + 1, int(x_min_max[1] / 10)))
            axis.set_xlim(x_min_max)
        if y_min_max:
            axis.set_ylim(y_min_max)
        axis.set_title(title)
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        ys = None
        for coordinate, name in zip(coordinates, mutation_names):
            xs = coordinate[0]
            ys = coordinate[1]
            if smooth:
                xs_smooth, ys_smooth = Figure.__smooth_line(coordinate)
                axis.plot(xs_smooth, ys_smooth, label=name)
            else:
                axis.plot(xs, ys, label=name)
            if show_dots:
                axis.plot(xs, ys, "ro")
        if threshold:
            if ys[-1] >= threshold:
                green = "#00f700"
                plt.axhline(y=threshold, color=green, linestyle='-')
            else:
                plt.axhline(y=threshold, color='r', linestyle='-')
        Figure.__add_information_box(axis, parameters, mutation_names)
        return figure

    @staticmethod
    def draw_bar_chart(data_sets, diagram_name, x_labels, title=None):
        n = len(data_sets)
        index = np.arange(len(data_sets[0][0]))
        width = 0.8 / len(data_sets)
        figure, axis = plt.subplots()
        if title is None:
            title = diagram_name
        axis.set_title(title)
        x_labels = Figure.shorten_traffic_signs(x_labels)
        axis.set_xticklabels(x_labels)
        plt.xticks(index + width * (n - 1) / 2, x_labels, rotation="vertical")
        rects = ()
        labels = ()
        for i in range(len(data_sets)):
            rect = axis.bar(index + i * width, data_sets[i][0], width)
            Figure.__auto_label(rect, axis)
            rects = (*rects, rect)
            labels = (*labels, data_sets[i][1])
        axis.legend(rects, labels, shadow=True)
        diagram_name = fm.build_name(diagram_name)
        return figure, diagram_name

    @staticmethod
    def shorten_traffic_signs(traffic_sign):
        short_forms = {"Zulässige Höchstgeschwindigkeit (20)": "Geschwindigkeitsbegrenzung 20",
                       "Zulässige Höchstgeschwindigkeit (30)": "Geschwindigkeitsbegrenzung 30",
                       "Zulässige Höchstgeschwindigkeit (50)": "Geschwindigkeitsbegrenzung 50",
                       "Zulässige Höchstgeschwindigkeit (60)": "Geschwindigkeitsbegrenzung 60",
                       "Zulässige Höchstgeschwindigkeit (70)": "Geschwindigkeitsbegrenzung 70",
                       "Zulässige Höchstgeschwindigkeit (80)": "Geschwindigkeitsbegrenzung 80",
                       "Zulässige Höchstgeschwindigkeit (100)": "Geschwindigkeitsbegrenzung 100",
                       "Zulässige Höchstgeschwindigkeit (120)": "Geschwindigkeitsbegrenzung 120",
                       "Ende der Geschwindigkeitsbegrenzung (80)": "80 aufgehoben",
                       "Überholverbot für Kraftfahrzeuge aller Art": "Überholverbot",
                       "Überholverbot für Kraftfahrzeuge mit einer zulässigen Gesamtmasse über 3,5t":
                           "Überholverbot (3,5t)",
                       "Ende des Überholverbotes für Fahrzeuge aller Art": "Überholverbot aufgehoben",
                       "Ende des Überholverbotes für Kraftfahrzeuge mit einer zulässigen Gesamtmasse über 3,5t":
                       "Überholverbot aufgehoben (3,5t)",
                       "Verbot für Kraftfahrzeuge mit einer zulässigen Gesamtmasse von 3,5t":
                           "Verbot für Fahrzeuge (3,5t)",
                       "Schleudergefahr bei Nässe oder Schmutz": "Schleudergefahr",
                       "Einseitig(rechts) verengte Fahrbahn": "Verengte Fahrbahn (rechts)"
                       }
        shortened_traffic_signs = []
        for x_label in traffic_sign:
            if str(x_label) in short_forms:
                x_label = short_forms[x_label]
            shortened_traffic_signs.append(x_label)
        return shortened_traffic_signs

    @staticmethod
    def __smooth_line(data_set):
        xs = np.array(data_set[0])
        ys = np.array(data_set[1])
        if len(xs) >= 4:
            t, c, k = interpolate.splrep(xs, ys, s=0, k=3)
            spline = interpolate.BSpline(t, c, k, extrapolate=False)
            x_smooth = np.linspace(xs.min(), xs.max(), 100)
            y_smooth = spline(x_smooth)
            return x_smooth, y_smooth
        else:
            CustomWarnings.print_warning("Too less data for a smooth curve.")
            return xs, ys

    @staticmethod
    def __add_information_box(axis, parameters, mutation_names):
        if parameters is None:
            if not mutation_names == [""]:
                print(mutation_names)
                axis.legend(loc="best", shadow=True)
            return
        text = ""
        for parameter in parameters:
            text += Figure.beautify_string(str(parameter)) + " = " + \
                    Figure.beautify_string(str(parameters[parameter])) + "\n"
        text_box = AnchoredText(text.strip(), frameon=True, loc="upper left", borderpad=1)
        plt.setp(text_box.patch, facecolor="grey", alpha=0.3)
        axis.add_artist(text_box)

    @staticmethod
    def __auto_label(rects, axis):
        for rect in rects:
            height = rect.get_height()
            axis.text(rect.get_x() + rect.get_width() / 2., height,
                      Figure.beautify_string("%.1f" % height),
                      ha="center", va="bottom")
