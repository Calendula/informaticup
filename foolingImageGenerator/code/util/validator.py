""" This module can be used to validate parameters in the context of this project.
"""

from util.customWarnings import CustomWarnings
from enums.parameter import Parameter
from enums.mutation import Mutation
from util.fileManager import FileManager as fm


class Validator:
    _DEFAULT_PARENT_PARAMS = Parameter.get_default_parent_parameters()
    _MIN_ANALYSIS_MAX_GENERATION_NO = 2
    CONFIG_MODE = "mode"
    CONFIG_MAIN = "main"
    CONFIG_ANALYSIS = "analysis"

    @staticmethod
    def validate_numeric_parameter_test_params(mutation_name, parameter_name, initial_val, final_val, step_size):
        mutation = Mutation(mutation_name)
        if Validator.__validate_mutation(mutation):
            if parameter_name in Parameter.get_default_parameters_for_mutation(mutation) or \
                    parameter_name == Parameter.SUCCESSORS_COUNT.get_name():
                validate_initial_value = Validator.__validate_parameter(parameter_name, initial_val, get_default=False)
                validate_final_value = Validator.__validate_parameter(parameter_name, final_val, get_default=False)
                validate_step_size_value = Validator.__validate_parameter(parameter_name, step_size, get_default=False)
                if validate_initial_value and validate_final_value and validate_step_size_value:
                    return True
                else:
                    invalid_params = {}
                    if not validate_initial_value:
                        invalid_params["initial_value"] = initial_val
                    if not validate_final_value:
                        invalid_params["final_value"] = final_val
                    if not validate_step_size_value:
                        invalid_params["step_size"] = step_size
                    msg = CustomWarnings.build_message_invalid_parameter_test_params(parameter_name, **invalid_params)
                    CustomWarnings.print_warning(msg)
            else:
                message = CustomWarnings.build_message_invalid_parameter_test_name(parameter_name, mutation)
                CustomWarnings.print_warning(message)
        return False

    @staticmethod
    def validate_compare_mutations(mutation_names, max_generation_no):
        valid = True
        if not Validator.__validate_max_generation_no_for_analysis(max_generation_no):
            valid = False
        for mutation_name in mutation_names:
            mutation = Mutation(mutation_name)
            if not Validator.__validate_mutation(mutation):
                valid = False
        return valid

    @staticmethod
    def validate_determine_key_figures(mutation_names, max_generation_no):
        if not Validator.__validate_max_generation_no_for_analysis(max_generation_no):
            return False
        for mutation_name in mutation_names:
            mutation = Mutation(mutation_name)
            if not Validator.__validate_mutation(mutation):
                return False
        return True

    @staticmethod
    def validate_evolution(mutation, parent_and_mutation_parameters, **evolution_parameters):
        mutation = Validator.__get_validated_mutation(mutation)

        validated_evolution_params = []
        for key in evolution_parameters:
            validated_evolution_params.append(Validator.__validate_parameter(key, evolution_parameters[key]))

        validated_parent_and_mutation_params = \
            Validator.__validate_parent_and_mutation_parameters(mutation, **parent_and_mutation_parameters)
        return mutation, validated_parent_and_mutation_params, tuple(validated_evolution_params)

    @staticmethod
    def validate_evolution_with_strings(successors_count, max_generation_no, allow_downswing, confidence_threshold,
                                        mutation, parameters):
        successors_count, max_generation_no, allow_downswing, confidence_threshold = \
            Validator.__validate_evolution_parameters_as_strings(successors_count, max_generation_no, allow_downswing,
                                                                 confidence_threshold)
        mutation, parameters = Validator.__validate_parent_and_mutation_parameters_as_strings(mutation, parameters)
        return successors_count, max_generation_no, allow_downswing, confidence_threshold, mutation, parameters

    @staticmethod
    def validate_evolution_config_file(config_json):
        if Validator.CONFIG_MODE not in config_json.keys():
            CustomWarnings.print_warning(f"Config file 'evolution_config.json' contains errors."
                                         f" The key '{Validator.CONFIG_MODE}' is missing. Program aborted.")
            return False
        else:
            mode = config_json[Validator.CONFIG_MODE]
            if mode in [Validator.CONFIG_ANALYSIS, Validator.CONFIG_MAIN]:
                if mode not in config_json.keys():
                    CustomWarnings.print_warning(f"Config file 'evolution_config.json' contains errors. "
                                                 f"The key '{mode}' is missing for mode '{mode}'. Program aborted.")
                    return False
                elif not isinstance(config_json[mode], list):
                    CustomWarnings.print_warning(f"Config file 'evolution_config.json' contains errors. "
                                                 f"The value '{config_json[mode]}' is not a valid value"
                                                 f" for mode '{mode}'. Value must be an array. Program aborted.")
                    return False
            else:
                CustomWarnings.print_warning(f"Config file 'evolution_config.json' contains errors. "
                                             f"The value '{mode}' is not a valid mode."
                                             f"Value must be '{Validator.CONFIG_ANALYSIS}' or "
                                             f"'{Validator.CONFIG_MAIN}'. Program aborted.")
                return False
        return True

    @staticmethod
    def convert_str_to_bool(string):
        return (string == str(True))

    @staticmethod
    def __validate_parent_and_mutation_parameters(mutation, **parameters):
        parent_parameters = {}
        mutation_parameters = {}
        for key in parameters:
            if key in Parameter.get_default_parent_parameters():
                parent_parameters[key] = parameters[key]
            else:
                mutation_parameters[key] = parameters[key]

        validated_parent_parameters = Validator.__validate_parent_parameters(**parent_parameters)
        validated_mutation_parameters = Validator.__validate_mutation_parameters(
            mutation, **mutation_parameters)

        validated_params = validated_parent_parameters.copy()
        validated_params.update(validated_mutation_parameters)
        return validated_params

    @staticmethod
    def __validate_mutation_parameters(mutation, **parameters):
        validated_params = {}
        for key in list(parameters):
            if key in Parameter.get_default_parameters_for_mutation(mutation):
                validated_params[key] = Validator.__validate_parameter(key, parameters[key])
            else:
                message = CustomWarnings.build_message_invalid_mutation_parameter_name(key, mutation)
                CustomWarnings.print_warning(message)
        return validated_params

    @staticmethod
    def __validate_parent_parameters(**parameters):
        validated_params = {}
        for key in list(parameters):
            validated_params[key] = Validator.__validate_parameter(key, parameters[key])
        return validated_params

    @staticmethod
    def __validate_evolution_parameters_as_strings(successors_count, max_generation_no, allow_downswing,
                                                   confidence_threshold):
        unconverted_evolution_parameters = {Parameter.SUCCESSORS_COUNT.get_name(): successors_count,
                                            Parameter.MAX_GENERATION_NO.get_name(): max_generation_no,
                                            Parameter.ALLOW_DOWNSWING.get_name(): allow_downswing,
                                            Parameter.CONFIDENCE_THRESHOLD.get_name(): confidence_threshold}

        validated_evolution_params = []
        for key in unconverted_evolution_parameters:
            param_value = unconverted_evolution_parameters[key]
            param_value = Validator.__convert_parameter_to_any_accepted_type(key, param_value)
            param_value = Validator.__validate_parameter(key, param_value, get_default=True, gui_message=True)
            validated_evolution_params.append(param_value)

        return tuple(validated_evolution_params)

    @staticmethod
    def __validate_parent_and_mutation_parameters_as_strings(mutation, parameters):
        mutation = Validator.__get_validated_mutation(mutation)
        for parameter_name in parameters:
            value = parameters[parameter_name]
            value = Validator.__convert_parameter_to_any_accepted_type(parameter_name, value)
            parameters[parameter_name] = Validator.__validate_parameter(parameter_name, value, True, True)
        return mutation, parameters

    @staticmethod
    def __convert_parameter_to_any_accepted_type(parameter_name, value):
        accepted_types = Parameter(parameter_name).get_accepted_types()
        for param_type in accepted_types:
            if param_type is bool:
                return Validator.convert_str_to_bool(value)
            elif param_type is None and value is None:
                return None
            elif param_type is not None:
                try:
                    value = param_type(value)
                    break
                except ValueError:
                    pass
        return value

    @staticmethod
    def __validate_parameter(parameter_name, input_value, get_default=True, gui_message=False):
        validator = {Parameter.PERCENT.get_name():
                         lambda x: Validator.__validate_restricted_float(x, 0, 100),
                     Parameter.CIRCLES_PER_ROUND.get_name(): Validator.__validate_positive_int,
                     Parameter.DIAMETER_TO_IMAGE_SIZE.get_name():
                         lambda x: Validator.__validate_restricted_float(x, 0, 100),
                     Parameter.FILLED.get_name(): Validator.__validate_bool,
                     Parameter.RECTANGLES_PER_ROUND.get_name(): Validator.__validate_positive_int,
                     Parameter.VERTICES.get_name(): Validator.__validate_positive_int,
                     Parameter.AREAS.get_name(): Validator.__validate_positive_int,
                     Parameter.POLYGONS_PER_ROUND.get_name(): Validator.__validate_positive_int,
                     Parameter.COLUMNS_AND_ROWS.get_name(): Validator.__validate_positive_int,

                     Parameter.PARENT_GRADIENT.get_name(): Validator.__validate_bool,
                     Parameter.PARENT_COLUMNS_AND_ROWS.get_name(): Validator.__validate_positive_int,
                     Parameter.PARENT_PATH.get_name(): Validator.__validate_image_path,

                     Parameter.SUCCESSORS_COUNT.get_name(): Validator.__validate_positive_int,
                     Parameter.MAX_GENERATION_NO.get_name(): Validator.__validate_positive_int_or_none,
                     Parameter.ALLOW_DOWNSWING.get_name(): Validator.__validate_bool,
                     Parameter.HIGH_QUALITY.get_name(): Validator.__validate_bool,
                     Parameter.SHOW_EVOLUTION_PROCESS.get_name(): Validator.__validate_bool,
                     Parameter.CONFIDENCE_THRESHOLD.get_name():
                         lambda x: Validator.__validate_restricted_float(x, 90, 100),
                     Parameter.COLUMNS_AND_ROWS.get_name(): Validator.__validate_positive_int,
                     Parameter.GRADIENT.get_name(): Validator.__validate_bool}

        default_value = Parameter(parameter_name).get_default_value()
        if get_default:
            if validator[parameter_name](input_value):
                return input_value
            else:
                message = CustomWarnings.build_message_invalid_parameter_value(parameter_name, input_value,
                                                                               default_value, gui_message)
                CustomWarnings.print_warning(message)
                return default_value
        else:
            return validator[parameter_name](input_value)

    @staticmethod
    def __validate_bool(input):
        if isinstance(input, bool):
            return True
        return False

    @staticmethod
    def __validate_positive_int(input):
        if isinstance(input, int):
            if input > 0:
                return True
        return False

    @staticmethod
    def __validate_positive_int_or_none(input):
        if isinstance(input, int):
            if input > 0:
                return True
        elif input is None:
            return True
        return False

    @staticmethod
    def __validate_restricted_float(input, lower_bound=0.0, upper_bound=1.0):
        if isinstance(input, int) or isinstance(input, float):
            if lower_bound <= input <= upper_bound:
                return True
        return False

    @staticmethod
    def __validate_image_path(path):
        if path is None or (fm.is_file_existent(path) and fm.is_image(path)):
            return True
        return False

    @staticmethod
    def __validate_max_generation_no_for_analysis(input):
        if isinstance(input, int) and input > Validator._MIN_ANALYSIS_MAX_GENERATION_NO:
            return True
        message = f"The parameter 'max_generation_no' must be an integer value which is greater than " \
            f"{str(Validator._MIN_ANALYSIS_MAX_GENERATION_NO - 1)}. Analysis was aborted."
        CustomWarnings.print_warning(message)
        return False

    @staticmethod
    def __validate_mutation(input):
        if isinstance(input, Mutation):
            return True
        else:
            message = CustomWarnings.build_message_invalid_mutation(input, test_abortion=True)
            CustomWarnings.print_warning(message)
            return False

    @staticmethod
    def __get_validated_mutation(input):
        if isinstance(input, Mutation):
            return input
        else:
            message = CustomWarnings.build_message_invalid_mutation(input, set_to_default=True)
            CustomWarnings.print_warning(message)
            return Parameter.MUTATION_NAME.get_default_value()
