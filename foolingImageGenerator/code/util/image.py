""" This module includes the class Image, which represents an image and possible ways to manipulate it.
"""

import math
import cv2
import numpy as np
import random
import util.fileManager as fileManager


class Image:
    SIZE_FINAL = 64
    SIZE = 512
    RGB_MAX = 256
    RGB_CONVERT = 255.0

    def __init__(self, given_image_path=None, width=SIZE, height=SIZE):
        if given_image_path:
            self.img = fileManager.FileManager.get_image_array_from_path(given_image_path, width, height)
            if self.img is None:
                self.img = Image.get_initial_image_array(width, height)
        else:
            self.img = Image.get_initial_image_array(width, height)

    @staticmethod
    def get_initial_image_array(width=SIZE, height=SIZE):
        img = np.zeros([height, width, 3])
        return img

    def set_random_color(self, parent_gradient, parent_columns_and_rows):
        if parent_gradient is False:
            column_width = int(Image.SIZE / parent_columns_and_rows)
            for m in range(0, parent_columns_and_rows):
                for n in range(0, parent_columns_and_rows):
                    x0 = m * column_width
                    x1 = x0 + column_width
                    y0 = n * column_width
                    y1 = y0 + column_width
                    self.img[x0:x1, y0:y1, :] = np.ones([column_width, column_width, 3]) * Image.get_random_color()
        else:
            self.__set_random_color_gradient(Image.SIZE, parent_columns_and_rows)
        self.img = Image.satisfy_float_value_range(self.img)

    def __set_random_color_gradient(self, width, parent_columns_and_rows=1):
        color_span = 1.0
        color_diff = color_span / width
        column_width = int(Image.SIZE / parent_columns_and_rows)
        for m in range(0, parent_columns_and_rows):
            for n in range(0, parent_columns_and_rows):
                offset_x = m * column_width
                offset_y = n * column_width
                color = Image.get_random_color()
                random_color_diff = Image.__get_random_color_diff(color_diff)

                for i in range(0, column_width, 1):
                    for j in range(0, column_width, 1):
                        self.set_color_for_pixel(i + offset_x, j + offset_y, color)
                        self.set_color_for_pixel(j + offset_x, i + offset_y, color)
                    color += random_color_diff

    def add_color_gradient(self, width=SIZE, offset_x=0, offset_y=0):
        color_span = 0.3
        color_diff = color_span / width
        random_color_diff = Image.__get_random_color_diff(color_diff)
        color = np.copy(random_color_diff)

        for i in range(0, width, 1):
            for j in range(0, width):
                self.img[i + offset_x, j + offset_y] += color
                self.img[j + offset_x, i + offset_y] += color
            color += random_color_diff

    def add_color(self, width=SIZE, offset_x=0, offset_y=0):
        color_span = 0.3
        random_color_diff = Image.__get_random_color_diff(color_span)
        self.img[offset_x:offset_x + width, offset_y:offset_y + width] += random_color_diff

    def set_random_color_for_pixel(self, x, y, pixel_width=1):
        delta = pixel_width
        self.img[x:x+delta, y:y+delta, :] = np.ones([3]) * Image.get_random_color()

    def set_color_for_pixel(self, x, y, color):
        self.img[x, y, :] = color

    @staticmethod
    def get_random_point():
        x = Image.__get_random_coordinate()
        y = Image.__get_random_coordinate()
        point = (x, y)
        return point

    @staticmethod
    def convert_polar_coordinates_to_point(radian, radial_coordinate):
        x = radial_coordinate * math.cos(radian)
        y = radial_coordinate * math.sin(radian)
        boundary = (Image.SIZE - 1) / 2.0
        x = max(-boundary, min(x, boundary))
        y = max(-boundary, min(y, boundary))
        x += boundary
        y += boundary
        point = (round(x), round(y))
        return point

    @staticmethod
    def get_random_color():
        b = Image.__get_random_color_value()
        g = Image.__get_random_color_value()
        r = Image.__get_random_color_value()
        bgr = (b, g, r)
        return bgr

    @staticmethod
    def satisfy_float_value_range(arr):
        return np.clip(arr, 0.0, 1.0)

    @staticmethod
    def __convert_bgr_to_rgb(bgr):
        rgb = bgr[..., ::-1]
        return rgb

    @staticmethod
    def convert_image_array_for_presentation(bgr_image):
        image_with_int_value_range = Image.convert_image_array_to_int_value_range(bgr_image)
        rgb_image = Image.__convert_bgr_to_rgb(image_with_int_value_range)
        return rgb_image

    @staticmethod
    def convert_image_array_to_int_value_range(image):
        copy = np.uint8(image * Image.RGB_CONVERT)
        return copy

    @staticmethod
    def resize(image, width, height=None):
        if height is None:
            height = width
        if np.size(image, 0) > width or np.size(image, 1) > height:
            image = cv2.resize(image, (width, height),
                               interpolation=cv2.INTER_CUBIC)
        elif np.size(image, 0) < width or np.size(image, 1) > height:
            image = cv2.resize(image, (width, height),
                               interpolation=cv2.INTER_AREA)
        return image

    @staticmethod
    def __get_random_coordinate():
        return random.randint(0, Image.SIZE)

    @staticmethod
    def __get_random_color_value():
        return random.randint(0, Image.RGB_MAX) / Image.RGB_CONVERT

    @staticmethod
    def __get_random_color_diff(color_span):
        random_b = random.uniform(-color_span, color_span)
        random_g = random.uniform(-color_span, color_span)
        random_r = random.uniform(-color_span, color_span)
        random_color_diff = np.array([random_b, random_g, random_r])
        return random_color_diff
