"""This package includes all classes which serve general purposes like managing files or representing an image.

Submodules
==========

.. autosummary::

    customWarnings
    figure
    fileManager
    image
    validator
"""
