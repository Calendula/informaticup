"""This module includes the enum for mutation which can be used for an evolution.
"""

from enum import Enum


class Mutation(Enum):
    """ Enum for possible mutation modes."""

    RANDOM_PIXELS = "random_pixels"
    """Change pixels to a random color."""
    DRAW_CIRCLES = "draw_circles"
    """Draw circles onto the image."""
    DRAW_RECTANGLES = "draw_rectangles"
    """Draw rectangles onto the image."""
    POINT_SYMMETRIC_POLYGONS = "point_symmetric_polygons"
    """Split the image into radial areas and draw polygons onto the image."""
    COLOR_GRID = "color_grid"
    """Lay a colored grid over the image."""
