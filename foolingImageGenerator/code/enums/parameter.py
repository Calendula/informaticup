"""This module includes the enum which comprises all parameters for an evolution, which can be defined by the user.
"""

from enum import Enum
from enums.mutation import Mutation


class Parameter(Enum):
    """
        Enum which specifies all parameters for an evolution.

        The class specifies all parameters which are needed to start an evolution. For each parameter its name, default
        value, type and in certain cases an alternative name are specified. The default values are used in case no value
        is specified. The default values are determined based on analysis and experience to cause the best possible
        result.

        The parameters can be classified into three categories. Parameters for:

            - evolution:        apply to the whole evolution (e.g. mutation mode)
            - parent:           used for generating the first generation if no parent is specified
            - mutation:         apply to the chosen mutation which is used to develops succeeding generations

        In the `See also`-section the methods which document each category are listed.


        See also
        --------
        evol.evolution.Evolution.start: starts an evolution. Documentation for evolution parameters.
        evol.generation.Generation._Generation__generate_parent_image: generates the parent. Documentation for parent parameters.
        evol.geneticOperator.GeneticOperator.mutate: mutation mode to create an individual out of a parent. Follow the
            `See also`- section for the documentation of mutation parameters with regard to the mutation mode.

    """
    # Evolution
    MUTATION_NAME = "mutation_name", Mutation.COLOR_GRID.value, [Mutation], "Mutation"
    SUCCESSORS_COUNT = "successors_count", 5, [int]
    MAX_GENERATION_NO = "max_generation_no", None, [None, int], "Number Generations"
    ALLOW_DOWNSWING = "allow_downswing", True, [bool], "Allow Downswings"
    HIGH_QUALITY = "high_quality", False, [bool]
    CONFIDENCE_THRESHOLD = "confidence_threshold", 95, [float, int]
    PARENT_PATH = "parent_path", None, [None, str], "Parent image"
    SHOW_EVOLUTION_PROCESS = "show_evolution_process", True, [bool]

    # Parent
    PARENT_GRADIENT = "parent_gradient", False, [bool]
    PARENT_COLUMNS_AND_ROWS = "parent_columns_and_rows", 1, [int]
    PATH = "path", None, [str]

    # Mutation
    PERCENT = "percent", 3, [float, int]
    DIAMETER_TO_IMAGE_SIZE = "diameter_to_image_size", 70, [float, int]
    CIRCLES_PER_ROUND = "circles_per_round", 3, [int]
    RECTANGLES_PER_ROUND = "rectangles_per_round", 4, [int]
    FILLED = "filled", True, [bool]
    COLUMNS_AND_ROWS = "columns_and_rows", 3, [int]
    GRADIENT = "gradient", False, [bool]
    POLYGONS_PER_ROUND = "polygons_per_round", 1, [int]
    AREAS = "areas", 2, [int]
    VERTICES = "vertices", 5, [int]
    RATIO_MAX_LINE_THICKNESS_TO_RADIUS = "ratio_max_line_thickness_to_radius", 2, [int]
    MAX_LINE_THICKNESS_FACTOR = "max_line_thickness_factor", 0.05, [float, int]

    def __new__(cls, alias, default_value, parameter_type, label_name=None):
        obj = object.__new__(cls)
        if label_name is None:
            label_name = alias
        label_name = label_name.replace("_", " ").replace(".", ",").title()
        obj._value_ = [alias, default_value, parameter_type, label_name]
        cls._value2member_map_[alias] = obj
        return obj

    def get_name(self):
        return self.value[0]

    def get_default_value(self):
        return self.value[1]

    def get_accepted_types(self):
        return self.value[2]

    def get_label_name(self):
        return self.value[3]

    @classmethod
    def get_default_parameters(cls, parameters):
        default_parameters = {}
        for parameter in parameters:
            default_parameters[parameter.get_name()] = parameter.get_default_value()
        return default_parameters

    @staticmethod
    def get_default_parameters_for_mutation(mutation):
        """
            Returns the default parameters (name and value) for a specified mutation mode.

            The method takes a `mutation` mode and returns the default parameters specified for that mutation mode.
            The parameters are added to a dictionary with their name as key and their default value as value.
            The dictionary is returned.


            Parameters
            ----------
            mutation : Mutation
                Mutation which is applied to the picture. Element of the Enum Mutation.
                The mutation should be written in the form Enum.MODE e.g.: `Mutation.RANDOM_PIXELS`.

            Returns
            -------
            default_parameters: dictionary
                Dictionary which consists of the default parameters of the specified mutation. The parameter names are
                keys and the default values are values.

        """
        mutation_parameters = Parameter.__get_parameters_for_mutation(mutation)
        default_parameters = Parameter.get_default_parameters(mutation_parameters)
        return default_parameters

    @staticmethod
    def get_default_parent_parameters():
        parameters = [Parameter.PARENT_GRADIENT, Parameter.PARENT_COLUMNS_AND_ROWS, Parameter.PARENT_PATH]
        return Parameter.get_default_parameters(parameters)

    @staticmethod
    def __get_parameters_for_mutation(mutation):
        params_for_mutation = {Mutation.RANDOM_PIXELS: [Parameter.PERCENT],
                               Mutation.DRAW_CIRCLES: [Parameter.CIRCLES_PER_ROUND, Parameter.DIAMETER_TO_IMAGE_SIZE,
                                                       Parameter.FILLED,
                                                       Parameter.RATIO_MAX_LINE_THICKNESS_TO_RADIUS],
                               Mutation.DRAW_RECTANGLES: [Parameter.RECTANGLES_PER_ROUND,
                                                          Parameter.MAX_LINE_THICKNESS_FACTOR,
                                                          Parameter.FILLED],
                               Mutation.POINT_SYMMETRIC_POLYGONS: [Parameter.VERTICES, Parameter.AREAS,
                                                                   Parameter.POLYGONS_PER_ROUND],
                               Mutation.COLOR_GRID: [Parameter.COLUMNS_AND_ROWS, Parameter.GRADIENT]}
        return params_for_mutation[mutation]

