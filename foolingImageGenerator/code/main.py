""" This module can be used to start an evolution.
"""

from evol.evolution import Evolution
from enums.mutation import Mutation
from util.validator import Validator
from enums.parameter import Parameter
import random


class Main:
    """
        The class allows to start evolutions.

        To start an evolution, an instance of class `Evolution` has to be created. By calling the method `start`, the
        evolution is started. It is possible to restart the evolution for an instance by calling the method `start`
        again. For each run of the evolution several parameters can be specified which influence the evolution. All
        parameters are optional, since for each of them a default parameter is defined which will be used in that case.

        The parameters can be classified into three categories. Parameters for:

            - evolution:        apply to the whole evolution (e.g. mutation mode)
            - parent:           used to generate a parent if no parent is specified
            - mutation:         apply to the chosen mutation which is used to develop succeeding generations

        In the `See also`-section the methods are listed which document each category.

        In order to follow the evolution process, the maximum confidence achieved for each generation is printed to the
        console. This is for each generation: The generation number, the confidence of the fittest individual and
        the confidence of the current parent (in case the parameter `allow_downswing` is set to `True`).
        The Example-section shows what the output looks like.


        See also
        --------
        evol.evolution.Evolution.start: starts an evolution. Documentation for evolution parameters.
        evol.generation.Generation._Generation__generate_parent_image: generates the parent. Documentation for parent
            parameters.
        evol.geneticOperator.GeneticOperator.mutate: mutation to create an individual out of a parent.
            Follow the `See also`-section for the documentation of mutation parameters with regard to the mutation mode.
        enums.parameter.Parameter: specifies each parameter and its default value


        Examples
        --------
        Start an evolution without specifying any parameters. Default parameters will be used.

        >>> evolution = Evolution
        >>> evolution.start()
        Mutation: color_grid
        Generation: 0
        Best Successor: 0.12340658
        Traffic sign: Rechts vorbei

        Generation: 1
        Best Successor: 0.57275867
        Traffic sign: Rechts vorbei
        ...

        The according `evolution_config.json` looks like this:

         .. code-block:: JSON

                {
                  "mode": "main",
                  "main": [ {} ]
                }

        Start an evolution with specifying some parameters. For other parameters the default values will be used. Since
        `allow_downswing` is set to False, the output will look different.

        >>> evolution = Evolution
        >>> evolution.start(mutation=Mutation.DRAW_CIRCLES.value, circles_per_round=8, allow_downswing=False)
        Mutation: draw_circles
        Generation: 0
        Best Successor: 0.06890261
        Traffic sign: Vorfahrt
        Parent: 0

        Generation: 1
        Best Successor: 0.47141826
        Traffic sign: Vorfahrt
        Parent: 0.06890261
        ...

        The according `evolution_config.json` looks like this:

         .. code-block:: JSON

                {
                  "mode": "main",
                  "main": [
                             {
                               "mutation_name": "draw_circles",
                               "circles_per_round": 8,
                                "allow_downswing": false
                             }
                          ]
                }
    """

    @staticmethod
    def run(config):
        evolutions = config[Validator.CONFIG_MAIN]
        for parameters in evolutions:
            evolution = Evolution()
            evolution.start(**parameters)

