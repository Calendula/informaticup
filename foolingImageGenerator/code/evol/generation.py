"""This module implements the class Generation.
"""

from evol.individual import Individual
from evol.geneticOperator import GeneticOperator
from connection import Connection
from util.fileManager import FileManager as fm
from enums.dataType import DataType
from enums.parameter import Parameter


class Generation:
    _PARENT_PATH = Parameter.PARENT_PATH.get_default_value()
    _PARENT_GRADIENT = Parameter.PARENT_GRADIENT.get_default_value()
    _PARENT_COLUMNS_AND_ROWS = Parameter.PARENT_COLUMNS_AND_ROWS.get_default_value()

    def __init__(self, generation_no, successors_count, evolution_metadata):
        self.map_pop_fitness = []
        self.successors_count = successors_count
        self._evolution_metadata = evolution_metadata
        self._confidence_threshold = evolution_metadata[Parameter.CONFIDENCE_THRESHOLD.get_name()]
        self.generation_no = generation_no
        self.number_fooling_images = 0
        self._high_quality = None

    def generate_successors(self, mutation, high_quality, parent, parent_path=_PARENT_PATH,
                            parent_gradient=_PARENT_GRADIENT, parent_columns_and_rows=_PARENT_COLUMNS_AND_ROWS,
                            **parameters):
        """
            Generates the individuals of the generation.

            The method uses the `parent` image and applies the specified `mutation` to a copy of it.
            In that way `self.successors_count` images are generated. If parent is not specified,
            it means the generation is the very first of an evolution.
            In that case a start-image will be generated, which will serve as first parent.

            Parameters
            ----------
            mutation : Mutation
                Mutation mode used for developing a generation out of a parent.
            high_quality: bool
                Determines whether each fooling image will be saved in its original size in addition to the 64x64-image,
                which is sent to the server.
            parent: Image
                If specified (not None), `parent` will be used as base to develop the first generation. If not, a
                parent is generated.
            parent_path: str, optional
                Parameter for the generation of the first parent. Relative or absolute path to a PNG- or JPEG-image
                which will serve as parent.
            parent_gradient: bool, optional
                Parameter for the generation of the first parent. Only used if `parent_path` is not specified.
                Specifies whether the image has a color gradient instead of a constant color.
            parent_columns_and_rows: int, optional
                Parameter for the generation of the first parent. Only used if `parent_path` is not specified.
                Specifies the number of areas, into which the image is divided. The image is divided into as many
                rows and columns as stated by the value of this parameter. Each area will be initialized with
                a different color.
            parameters: keyword arguments
                Optional parameters, which depend on the chosen mutation mode.
                See GeneticOperator.mutate for more information.
        """
        self._high_quality = high_quality
        if parent is None:
            self.__generate_parent_image(parent_path, parent_gradient, parent_columns_and_rows)
        else:
            for i in range(self.successors_count):
                img = GeneticOperator.mutate(mutation=mutation, origin_img=parent, generation_no=self.generation_no,
                                             individual_no=i, **parameters)
                self.map_pop_fitness.append([img, 0])
                fm.save(DataType.IMAGE, img, img.image_name)

    def __generate_parent_image(self, parent_path, parent_gradient, parent_columns_and_rows):
        """
            Generates the start-image.

            This method creates the start-image for an evolution. The created image will serve as parent for the first
            generation.

            There are different parent options:

                - user defined image: `parent_path` is set
                - unicolor image: `parent_columns_and_rows` is set to 1 and only one color will be set for the image.
                - grid image: `parent_columns_and_rows` is greater than 1. This results in a grid with as many
                  columns and rows as the value of `parent_columns_and_rows`.
                - pixel image: `parent_columns_and_rows` is set to the size of the image (64).


            Parameters
            ----------
            parent_gradient: bool
                Parameter for the generation of the parent. Specifies whether the images has a color gradient
                instead of a constant color.
            parent_columns_and_rows: int
                Parameter for the generation of the first parent. Specifies the number of areas, into which the
                image is divided. The image is divided into as many rows and columns as stated by the value of this
                parameter. Each area will be initialized with a different color.
        """
        if parent_path:
            individual = Individual(given_image_path=parent_path)
        else:
            individual = Individual()
            individual.set_random_color(parent_gradient, parent_columns_and_rows)
        fm.save(DataType.IMAGE, individual, individual.image_name)
        self.map_pop_fitness.append([individual, 0])

    def evaluate_fitness(self):
        con = Connection()
        for i in range(len(self.map_pop_fitness)):
            con.post(self.map_pop_fitness[i][0].image_name)
            self.map_pop_fitness[i][1] = con.get_highest_confidence()
            self.map_pop_fitness[i][0].add_request_data_to_metadata(con.get_highest_traffic_sign(),
                                                                    con.get_highest_confidence())
            if self.map_pop_fitness[i][0].is_fooling_image(self._confidence_threshold):
                self.number_fooling_images += 1
                traffic_sign = con.get_highest_traffic_sign()
                confidence = con.get_highest_confidence()
                image_name = fm.build_name(fm.build_name(traffic_sign, confidence))
                metadata = fm.build_metadata_for_file(self._evolution_metadata,
                                                      self.map_pop_fitness[i][0].image_metadata)
                fm.save(DataType.FOOLING_IMAGE,  self.map_pop_fitness[i][0], image_name, folder_name=traffic_sign,
                        metadata=metadata, high_quality=self._high_quality)

    @staticmethod
    def max_fitness(individual1, individual2):
        if individual1[1] > individual2[1]:
            return individual1
        return individual2

    def select_fittest(self):
        self.evaluate_fitness()
        maximum = self.map_pop_fitness[0]
        for creature in self.map_pop_fitness:
            maximum = self.max_fitness(creature, maximum)
        return maximum
