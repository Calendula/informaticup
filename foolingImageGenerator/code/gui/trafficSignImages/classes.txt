Class	Images	Name
0	x	Zul�ssige H�chstgeschwindigkeit (20)
1	x	Zul�ssige H�chstgeschwindigkeit (30)
2	x	Zul�ssige H�chstgeschwindigkeit (50)
3	x	Zul�ssige H�chstgeschwindigkeit (60)
4	x	Zul�ssige H�chstgeschwindigkeit (70)
5	x	Zul�ssige H�chstgeschwindigkeit (80)
6	x	Ende der Geschwindigkeitsbegrenzung (80)
7	x	Zul�ssige H�chstgeschwindigkeit (100)
8	x	Zul�ssige H�chstgeschwindigkeit (120)
9	x	�berholverbot f�r Kraftfahrzeuge aller Art
10	x	�berholverbot f�r Kraftfahrzeuge mit einer zul�ssigen Gesamtmasse �ber 3,5t
11	x	Einmalige Vorfahrt
12	x	Vorfahrt
13	x	Vorfahrt gew�hren
14	x	Stoppschild
15	x	Verbot f�r Fahrzeuge aller Art
16	x	Verbot f�r Kraftfahrzeuge mit einer zul�ssigen Gesamtmasse von 3,5t
17	x	Verbot der Einfahrt
18	x	Gefahrenstelle
19	x	Kurve (links)
20		Kurve (rechts)
21	-	Doppelkurve (zun�chst links)
22	x	Unebene Fahrbahn
23	x	Schleudergefahr bei N�sse oder Schmutz
24		Einseitig (rechts) verengte Fahrbahn
25	x	Baustelle
26		Lichtzeichenanlage
27	-	Fu�g�nger
28		Kinder
29	x	Fahrradfahrer
30		Schnee- oder Eisgl�tte
31	x	Wildwechsel
32	x	Ende aller Streckenverbote
33	x	Ausschlie�lich rechts
34		Ausschlie�lich links
35	x	Ausschlie�lich geradeaus
36		Geradeaus oder rechts
37		Geradeaus oder links
38	x	Rechts vorbei
39		Links vorbei
40	x	Kreisverkehr
41		Ende des �berholverbotes f�r Fahrzeuge aller Art
42	x	Ende des �berholverbotes f�r Kraftfahrzeuge mit einer zul�ssigen Gesamtmasse �ber 3,5t
