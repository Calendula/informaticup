import unittest
from connection import Connection
from util.fileManager import FileManager as fm
from enums.dataType import DataType


class ConnectionTestCase(unittest.TestCase):
    def setUp(self):
        file_name_mock_data = fm.get_relative_path_for_data_type(DataType.TEST_DATA,
                                                                 ["example_data_for_connection.json"])
        self.mock_data = fm.open_json_file(file_name_mock_data)
        self.connection = Connection()

        """def test_valid_requests(self):
        for request in self.mock_data["valid_requests"]:
            image_name = fm.get_relative_path_for_data_type(DataType.TEST_DATA, [request["image_name"]])
            self.connection.post(image_name, custom_folder=True)
            self.assertEqual(self.connection.get_highest_traffic_sign(), request["traffic_sign"],
                             'incorrect traffic sign responded')
            self.assertEqual(self.connection.get_highest_confidence(), request["confidence"],
                             'incorrect confidence responded')
            self.assertEqual(self.connection.get_status_code(), 200, "incorrect status code responded; 200 expected")

    def test_requests_with_invalid_image_names(self):
        for request in self.mock_data["invalid_image_names"]:
            image_name = fm.get_relative_path_for_data_type(DataType.TEST_DATA, [request["image_name"]])
            with self.assertRaises(Exception):
                self.connection.post(image_name, custom_folder=True)"""

    def test_invalid_requests(self):
        for request in self.mock_data["invalid_requests"]:
            #image_name = fm.get_relative_path_for_data_type(DataType.TEST_DATA, [request["image_name"]])
            image_name = "../../tests/testData/example_data_for_connection.json"
            with self.assertWarns(UserWarning):
                self.connection.post(image_name, custom_folder=True)
                self.assertEqual(self.connection.get_status_code(), 400, "incorrect status code responded")

    def tearDown(self):
        pass


if __name__ == "__main__":
    unittest.main()
