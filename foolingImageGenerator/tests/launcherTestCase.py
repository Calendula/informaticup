import unittest
import random
from util.fileManager import FileManager
from enums.dataType import DataType
from launcher import Launcher


class LauncherTestCase(unittest.TestCase):
    def setUp(self):
        FileManager.MODE = "test"
        random.seed(1337)
        self.path = FileManager.get_relative_path_for_data_type(DataType.TEST_DATA, ["launcher"])

    def test_start_main(self):
        Launcher.CONFIG_FILE = FileManager.build_relative_path([self.path, "CorrectDefaultEvolution.json"])
        try:
            launcher = Launcher()
        except Exception:
            self.fail("Launcher failed to start default Main")

    def test_start_analysis(self):
        Launcher.CONFIG_FILE = FileManager.build_relative_path([self.path, "ShortAnalysis.json"])
        try:
            launcher = Launcher()
        except Exception:
            self.fail("Launcher failed to start Analysis")

    def test_invalid_mode(self):
        Launcher.CONFIG_FILE = FileManager.build_relative_path([self.path, "invalidMode.json"])
        with self.assertWarnsRegex(UserWarning, "not a valid mode"):
            launcher = Launcher()

    def test_analysis_key_missing(self):
        Launcher.CONFIG_FILE = FileManager.build_relative_path([self.path, "AnalysisMissing.json"])
        with self.assertWarnsRegex(UserWarning, "The key 'analysis' is missing for mode"):
            launcher = Launcher()

    def test_main_key_missing(self):
        Launcher.CONFIG_FILE = FileManager.build_relative_path([self.path, "MainMissing.json"])
        with self.assertWarnsRegex(UserWarning, "The key 'main' is missing for mode"):
            launcher = Launcher()

    def test_invalid_config_file(self):
        Launcher.CONFIG_FILE = FileManager.build_relative_path([self.path, "MainMissing.json"])
        with self.assertWarnsRegex(UserWarning, "The key 'main' is missing for mode"):
            launcher = Launcher()

    def test_invalid_value_for_selected_mode(self):
        Launcher.CONFIG_FILE = FileManager.build_relative_path([self.path, "invalidValue.json"])
        with self.assertWarnsRegex(UserWarning, "is not a valid value"):
            launcher = Launcher()

    def test_invalid_value_for_selected_mode(self):
        Launcher.CONFIG_FILE = FileManager.build_relative_path([self.path, "ModeMissing.json"])
        with self.assertWarnsRegex(UserWarning, "'mode' is missing"):
            launcher = Launcher()

    def tearDown(self):
        FileManager.MODE = None
        pass


if __name__ == "__main__":
    unittest.main()
