FROM python:3

COPY requirements.txt ./

RUN pip3 install numpy requests boto3 opencv-python
RUN pip install --no-cache-dir -r requirements.txt

WORKDIR /app
COPY . .

ENV PATH=$PATH:/app/irrbilder/code
ENV PYTHONPATH /app/irrbilder/code	

WORKDIR /app/foolingImageGenerator/code
CMD [ "python", "./launcher.py" ]

