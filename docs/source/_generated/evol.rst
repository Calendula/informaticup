evol package
============

Submodules
----------

.. toctree::

   evol.evolution
   evol.generation
   evol.geneticOperator
   evol.individual

Module contents
---------------

.. automodule:: evol
    :members:
    :undoc-members:
    :show-inheritance:
